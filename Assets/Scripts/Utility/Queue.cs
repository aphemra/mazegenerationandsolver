﻿using System.Collections.Generic;

public class Queue<E> : IQueue<E> {
    private List<E> queue;
    private int size;

    public Queue()
    {
        queue = new List<E>();
        size = 0;
    }

    public int Size()
    {
        return size;
    }
    public bool IsEmpty()
    {
        return (size == 0);
    }

    public E Dequeue()
    {
        if (size == 0)
            return default(E);
        E temp = queue[size - 1];
        queue.RemoveAt(size - 1);
        size--;
        return temp;
    }
    public void Enqueue(E e)
    {
        queue.Insert(0, e);
        size++;
    }
    public E First()
    {
        if (size == 0)
            return default(E);
        return queue[size - 1];
    }

}
