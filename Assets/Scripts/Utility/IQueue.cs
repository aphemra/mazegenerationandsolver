﻿public interface IQueue<E>
{
    int Size();
    bool IsEmpty();
    E Dequeue();
    void Enqueue(E e);
    E First();
}
