﻿using UnityEngine;

public class CellFormat : MonoBehaviour {

    [Header("Cell Properties")]
    public Color wallColor;
    public Color openColor;
    public Color startColor;
    public Color endColor;
    public Color visitedColor;
    public Color pathColor;
}
