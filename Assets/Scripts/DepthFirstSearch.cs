﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepthFirstSearch : MonoBehaviour {

    public MazeGeneration mg;
    private List<Cell> cells;
    private Cell start, end;
    private Stack<Cell> solution; // Creates a stack to be used for holding cell information.
    private Cell currentCell;

    public void Search()
    {
        if (!mg.isDFSearched)
        {
            // Initial values
            cells = mg.cells;
            start = cells[mg.start];
            end = cells[mg.end];
            solution = new Stack<Cell>();

            Debug.Log("Start: " + start.name);
            Debug.Log("End: " + end.name);
            currentCell = start;
            solution.Push(currentCell);
            
            SearchNeighbors(currentCell);
            DrawSolution();
            mg.isBFSearched = !mg.isBFSearched;
            mg.isDFSearched = !mg.isDFSearched;
        }
    }

    private void SearchNeighbors(Cell cell)
    {
        if (!(cell.NorthCell.IsBlocked || cell.NorthCell.WasVisited || cell.NorthCell == null))
        {
            currentCell = cell.NorthCell;
            solution.Push(currentCell);
            currentCell.WasVisited = true;
            currentCell.FormatCell();
            if (currentCell != end)
                SearchNeighbors(currentCell);
            else
                return;
        }
        else if (!(cell.EastCell.IsBlocked || cell.EastCell.WasVisited || cell.EastCell == null))
        {
            currentCell = cell.EastCell;
            solution.Push(currentCell);
            currentCell.WasVisited = true;
            currentCell.FormatCell();
            if (currentCell != end)
                SearchNeighbors(currentCell);
            else
                return;
        }
        else if (!(cell.SouthCell.IsBlocked || cell.SouthCell.WasVisited || cell.SouthCell == null))
        {
            currentCell = cell.SouthCell;
            solution.Push(currentCell);
            currentCell.WasVisited = true;
            currentCell.FormatCell();
            if (currentCell != end)
                SearchNeighbors(currentCell);
            else
                return;
        }
        else if (!(cell.WestCell.IsBlocked || cell.WestCell.WasVisited || cell.WestCell == null))
        {
            currentCell = cell.WestCell;
            solution.Push(currentCell);
            currentCell.WasVisited = true;
            currentCell.FormatCell();
            if (currentCell != end)
                SearchNeighbors(currentCell);
            else
                return;
        } else
        {

            solution.Pop();
            if (solution.Size() == 0 || currentCell == null)
            {
                mg.noSolutionText.text = "No Solution!";
                return;
            }
            currentCell = solution.Top();
            if (currentCell != end)
                SearchNeighbors(currentCell);
            else
                return;
        }
    }

    private void DrawSolution()
    {
        int size = solution.Size();
        for (int i = 0; i < size; i++)
        {
            solution.Top().IsPath = true;
            solution.Top().FormatCell();
            solution.Pop();
        }
    }
}
