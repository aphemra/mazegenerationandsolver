﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreadthFirstSearch : MonoBehaviour
{
    public MazeGeneration mg;
    private List<Cell> cells;
    private Cell start, end;
    private Queue<Cell> solution;
    private Cell currentCell, selectedCell;

    public void Search()
    {
        if (!mg.isBFSearched)
        {
            // Initial values
            cells = mg.cells;
            start = cells[mg.start];
            end = cells[mg.end];
            solution = new Queue<Cell>();

            Debug.Log("Start: " + start.name);
            Debug.Log("End: " + end.name);
            currentCell = start;
            currentCell.Distance = 0;
            solution.Enqueue(currentCell);

            while (currentCell != end)
            {
                if (currentCell == null)
                {
                    mg.noSolutionText.text = "No Solution!";
                    break;
                }
                SearchNeighbors(currentCell);
                currentCell = solution.Dequeue();
            }
            if (end.Distance > 0)
                DrawSolution();
            else
                mg.noSolutionText.text = "No Solution!";
            mg.isBFSearched = !mg.isBFSearched;
            mg.isDFSearched = !mg.isDFSearched;
        }
    }

    private void SearchNeighbors(Cell cell)
    {
        
        if (!(cell.NorthCell.IsBlocked || cell.NorthCell.WasQueued || cell.NorthCell.WasVisited || cell.NorthCell == null))
        {
            selectedCell = cell.NorthCell;
            selectedCell.Distance = cell.Distance + 1;
            selectedCell.WasVisited = true;
            selectedCell.WasQueued = true;
            solution.Enqueue(selectedCell);
            selectedCell.FormatCell();
        }
        if (!(cell.EastCell.IsBlocked || cell.EastCell.WasQueued || cell.EastCell.WasVisited || cell.EastCell == null))
        {
            selectedCell = cell.EastCell;
            selectedCell.Distance = cell.Distance + 1;
            selectedCell.WasVisited = true;
            selectedCell.WasQueued = true;
            solution.Enqueue(selectedCell);
            selectedCell.FormatCell();
        }
        if (!(cell.SouthCell.IsBlocked || cell.SouthCell.WasQueued || cell.SouthCell.WasVisited || cell.SouthCell == null))
        {
            selectedCell = cell.SouthCell;
            selectedCell.Distance = cell.Distance + 1;
            selectedCell.WasVisited = true;
            selectedCell.WasQueued = true;
            solution.Enqueue(selectedCell);
            selectedCell.FormatCell();
        }
        if (!(cell.WestCell.IsBlocked || cell.WestCell.WasQueued || cell.WestCell.WasVisited || cell.WestCell == null))
        {
            selectedCell = cell.WestCell;
            selectedCell.Distance = cell.Distance + 1;
            selectedCell.WasVisited = true;
            selectedCell.WasQueued = true;
            solution.Enqueue(selectedCell);
            selectedCell.FormatCell();
        }
        return;
    }

    private void DrawSolution()
    {
        Cell currentCell = end;
        int check = 0;

        while (currentCell.Distance != 0 && check < 100000)
        {
            if (currentCell.NorthCell.Distance == currentCell.Distance - 1)
            {
                currentCell = currentCell.NorthCell;
                currentCell.IsPath = true;
                currentCell.FormatCell();
            }
            else if (currentCell.EastCell.Distance == currentCell.Distance - 1)
            {
                currentCell = currentCell.EastCell;
                currentCell.IsPath = true;
                currentCell.FormatCell();
            }
            else if (currentCell.SouthCell.Distance == currentCell.Distance - 1)
            {
                currentCell = currentCell.SouthCell;
                currentCell.IsPath = true;
                currentCell.FormatCell();
            }
            else if (currentCell.WestCell.Distance == currentCell.Distance - 1)
            {
                currentCell = currentCell.WestCell;
                currentCell.IsPath = true;
                currentCell.FormatCell();
            } check++;
        }
    }
}
