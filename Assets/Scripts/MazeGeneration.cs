﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class MazeGeneration : MonoBehaviour {

    [Header("Maze Constraints")]
    [Range(12, 36)]
    public int mazeWidth;
    [Range(12, 23)]
    public int mazeHeight;
    [Range(0, 1)]
    public float mazeDensity;

    [Header("Hooks and List Information")]
    public GameObject mazeParent;
    public SliderController sc;
    public TextMeshProUGUI noSolutionText;
    public Cell cell;
    public List<Cell> cells = new List<Cell>();

    private bool foundStart = false;
    private bool foundEnd = false;
    private bool isMazeSpawned = false;
    public bool isDFSearched = false;
    public bool isBFSearched = false;
    public int start, end;

    void Update()
    {
        mazeWidth = (int)sc.widthSlider.value;
        mazeHeight = (int)sc.heightSlider.value;
    }

    public void Generate()
    {
        if (isMazeSpawned)
        {
            CleanMaze();
            Generate();
        }
        else
        {
            GenerateBlankMaze(mazeWidth + 2, mazeHeight + 2);
            isMazeSpawned = !isMazeSpawned;
        }
    }

    private void GenerateBlankMaze(int mazeWidth, int mazeHeight)
    {
        mazeParent.GetComponent<GridLayoutGroup>().constraintCount = mazeHeight;
        for (int i = 0; i < mazeWidth; i++)
        {
            for (int j = 0; j < mazeHeight; j++)
            {
                Cell instantCell = Instantiate(cell, mazeParent.transform);
                instantCell.name = "Cell: [ " + i + ", " + j + " ]";
                instantCell.SetMazeSize(mazeWidth, mazeHeight);
                instantCell.X = i;
                instantCell.Y = j;
                instantCell.WasVisited = false;
                instantCell.IsBlocked = false;
                instantCell.IsStart = false;
                instantCell.IsEnd = false;
                instantCell.IsPath = false;
                instantCell.FormatCell();
                cells.Add(instantCell);
            }
        }
        for (int i = 0; i < cells.Count; i++)
        {
            AssociateCells(cells[i]);
            BuildWalls(cells[i], mazeDensity);
            SetStart(cells[i], i);
            SetEnd(cells[i], i);
        }
    }

    private void AssociateCells(Cell cell)
    {
        cell.NorthCell = cell.GetNorth(cell, cells);
        cell.EastCell = cell.GetEast(cell, cells);
        cell.WestCell = cell.GetWest(cell, cells);
        cell.SouthCell = cell.GetSouth(cell, cells);
    }

    private void BuildWalls(Cell cell, float mazeDensity)
    {
        if (cell.X == 0 || cell.Y == 0 || cell.X == mazeWidth + 1 || cell.Y == mazeHeight + 1 || Random.value < mazeDensity)
            cell.IsBlocked = true;
        cell.FormatCell();
    }

    private void SetStart(Cell cell, int i)
    {
        if (cell.IsBlocked == true || foundStart)
            return;

            if (cell.X < mazeWidth / 2 && cell.Y < mazeHeight / 2)
            {
                if (Random.value > .75f && foundStart == false)
                {
                    start = i;
                    cell.IsStart = true;
                    foundStart = true;
                }
            }
        cell.FormatCell();
    }

    private void SetEnd(Cell cell, int i)
    {
        if (cell.IsBlocked == true || foundEnd)
            return;
        if (cell.X >= mazeWidth / 2 && cell.Y >= mazeHeight / 2)
        {
            if (Random.value > .75f && foundEnd == false)
            {
                end = i;
                cell.IsEnd = true;
                foundEnd = true;
            }
        }
        cell.FormatCell();
    }

    private void CleanMaze()
    {
        foreach (Transform child in mazeParent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        noSolutionText.text = "";
        foundStart = false;
        foundEnd = false;
        cells = new List<Cell>();
        isMazeSpawned = !isMazeSpawned;
        isDFSearched = false;
        isBFSearched = false;
    }

    public void ReloadLevel()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}
