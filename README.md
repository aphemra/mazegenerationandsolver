This offers a imperfect maze generation through the instantiation of canvas UI prefab components.

In-project, it allows for the generation of different size mazes locked to a 720p window size.

It allows for re-generation of the maze to create different maze generations rapidly.

Uses the stack data structure in conjunction with depth first search to solve the maze.

Uses the queue data structure in conjunction with breadth first search to solve the maze.

Both solutions can be done, but not on the same maze.

Made in Unity 2018.2.14f